var exports = module.exports = {};
var getUserResources = require('./../other/getUserResources');

let models = require("../../models/index"),
    Users = models.users,
    Roles = models.roles,
    Resources = models.resources;

exports.index = function(req, res) {
    let _username = req.user.email.substring(0, req.user.email.indexOf("@"));

    Users.findOne({
        where: {id: req.user.id},
        include: { model: Roles, as: 'userHasRole', include: [{model: Resources, as: 'roleHasResource'}]}
    }).then((user) => {

        res.render('admin/index', {
            username: _username,
            title: 'Dashboard',
            admin_isDashboard: true,
            resourcesArray: getUserResources.getResources(user)
        });
    });
};