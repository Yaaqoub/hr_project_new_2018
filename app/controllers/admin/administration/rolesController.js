var exports = module.exports = {};
var getUserResources = require('./../../other/getUserResources');

let models = require("../../../models/index"),
    Users = models.users,
    Roles = models.roles,
    Resources = models.resources;

//let get_all_messages = require('../chat/getMessages');
let results = {};

exports.roles = function(req, res) {

    let _username = req.user.email.substring(0, req.user.email.indexOf("@"));

    Users.findOne({
        where: {id: req.user.id},
        include: { model: Roles, as: 'userHasRole', include: [{model: Resources, as: 'roleHasResource'}]}
    }).then((userResources) => {
        results.userResources = JSON.parse(JSON.stringify(userResources));

        return Resources.findAll();
    }).then((resResources) => {
        results.resResources = JSON.parse(JSON.stringify(resResources));
        return Roles.findAll({
            attributes: { include: [[models.sequelize.fn('DATE_FORMAT', models.sequelize.col('roles.created_at'), '%M %d %Y'), 'created_at'],
                    [models.sequelize.fn('DATE_FORMAT', models.sequelize.col('roles.updated_at'), '%M %d %Y'), 'updated_at']] },
            include: [{ model: Resources, as: 'roleHasResource' }]
        });
    }).then((resRoles) => {
        results.resRoles = JSON.parse(JSON.stringify(resRoles));
        res.render('admin/index', {
            username: _username,
            title: 'Administration',
            sub_title: 'Roles Management',
            admin_isRoleManagement: true,
            allRoles: results.resRoles,
            allResources: results.resResources,
            resourcesArray: getUserResources.getResources(results.userResources)
        });
    });
};

exports.createRole = function(req, res) {
    let getSubmit = req.body;
    let data = {
        role_name: getSubmit.role_name
    };

    Roles.create(data).then(function(newRole) {

        if (!newRole) {
            console.log("Can't create Role: " + data);
        }

        if (newRole) {
            res.redirect('/admin/users/roles');
        }
    });
};

exports.assignResources = function(req, res) {
    let getSubmit = req.body;

    Roles.findById(getSubmit.modal_role_id).then((role) => {
        if (Object.prototype.toString.call(getSubmit.resourcesName) === '[object String]') {
            Resources.findById(getSubmit.resourcesName).then((resource) => {
                role.addRoleHasResource(resource);
            });
        } else {
            for (let i = 0; i < getSubmit.resourcesName.length; i++) {
                Resources.findById(getSubmit.resourcesName[i]).then((resource) => {
                    role.addRoleHasResource(resource);
                });
            }
        }
    }).then(() => {
        res.redirect('/admin/users/roles');
    });
};