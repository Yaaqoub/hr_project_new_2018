var exports = module.exports = {};
var getUserResources = require('./../../other/getUserResources');

let models = require("../../../models/index"),
    Users = models.users,
    Roles = models.roles,
    Resources = models.resources;

//let get_all_messages = require('../chat/getMessages');

let results = {};

exports.users = function(req, res) {
    let _username = req.user.email.substring(0, req.user.email.indexOf("@"));

    let attributes = [[models.sequelize.fn('DATE_FORMAT', models.sequelize.col('users.created_at'), '%M %d %Y'), 'created_at'],
        [models.sequelize.fn('DATE_FORMAT', models.sequelize.col('users.updated_at'), '%M %d %Y'), 'updated_at'],
        [models.sequelize.fn('DATE_FORMAT', models.sequelize.col('users.last_login'), '%M %d %Y'), 'last_login']];

    Users.findOne({
        where: {id: req.user.id},
        include: { model: Roles, as: 'userHasRole', include: [{model: Resources, as: 'roleHasResource'}]}
    }).then((userResources) => {
        results.userResources = JSON.parse(JSON.stringify(userResources));

        return Roles.findAll();
    }).then((resRoles) => {
        results.resRoles = JSON.parse(JSON.stringify(resRoles));
        return Users.findAll({
            attributes: {include: attributes},
            include: [{ model: Roles, as: 'userHasRole' }]
        });
    }).then((resUsers) => {
        results.resUsers = JSON.parse(JSON.stringify(resUsers));

        res.render('admin/index', {
            username: _username,
            title: 'Administration',
            sub_title: 'Users Management',
            admin_isUserManagement: true,
            allUsers: results.resUsers,
            message: req.flash('signupMessage'),
            roles: results.resRoles,
            resourcesArray: getUserResources.getResources(results.userResources)
        });
    });
};

exports.addRole = function(req, res) {
    let getSubmit = req.body,
        removeButton = Object.keys(getSubmit)[2];


    //console.log("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");

    if (removeButton === 'removeButton') {
        Roles.findById(getSubmit.removeButton).then(function(role) {

            if (role) {
                Users.findById(getSubmit.user_id).then((user) => {
                    user.removeUserHasRole(role);
                    res.redirect('/admin/users');
                });
            }
        });
    } else {

        Roles.findById(getSubmit.role_name).then(function(role) {

            if (role) {
                Users.findById(getSubmit.user_id).then((user) => {
                    if (user) {
                        user.addUserHasRole(role);
                    }
                });
            }
        }).then(() => {
            res.redirect('/admin/users');
        });
    }
};

exports.actions = function(req, res) {
    let getSubmit = req.body,
        buttonsValue = Object.keys(getSubmit)[0],
        userID = getSubmit.user_id;

    let status = null;

    if (buttonsValue === '1')
        status = 'active';

    if (buttonsValue === '0')
        status = 'inactive';


    Users.update({
        status: status
    }, {
        where: {id: userID}
    }).then(() => {
        res.redirect('/admin/users');
    });
};