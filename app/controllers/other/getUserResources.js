module.exports = {
    getResources: function (user) {
        let resourcesObject = [];

        for (let i = 0; i < JSON.parse(JSON.stringify(user)).userHasRole.length; i++) {

            for (let j = 0; j < JSON.parse(JSON.stringify(user)).userHasRole[i].roleHasResource.length; j++) {
                let resource = JSON.parse(JSON.stringify(user)).userHasRole[i].roleHasResource[j];
                resourcesObject.push(resource.resource_name);
            }
        }

        return resourcesObject;
    }
};