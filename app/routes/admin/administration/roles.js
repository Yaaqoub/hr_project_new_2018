let rolesController = require('../../../controllers/admin/administration/rolesController.js');

module.exports = function(app) {

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();

        res.redirect('/login');
    }

    /**
     * (GET Method)
     */
    app.get('/admin/users/roles', isLoggedIn, rolesController.roles);

    app.post('/admin/users/roles/create', isLoggedIn, rolesController.createRole);

    app.post('/admin/users/roles/assign_resources', isLoggedIn, rolesController.assignResources);
};