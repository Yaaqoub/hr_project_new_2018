let usersController = require('../../../controllers/admin/administration/usersController.js');

module.exports = function(app) {

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();

        res.redirect('/login');
    }

    /**
     * (GET Method)
     */
    app.get('/admin/users', isLoggedIn, usersController.users);

    app.post('/admin/users/roles', isLoggedIn, usersController.addRole);

    app.post('/admin/users/actions', isLoggedIn, usersController.actions);
};