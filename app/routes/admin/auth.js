let authController = require('../../controllers/admin/authcontroller.js');

module.exports = function(app, passport) {

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();

        res.redirect('/login');
    }

    /**
     * Add new Admin (GET Method)
     */
    app.get('/admin_signup', isLoggedIn, authController.signup);

    /**
     * SignIn (GET Method)
     */
    app.get('/login', authController.signin);

    /**
     * Logout (GET Method)
     */
    app.get('/logout',authController.logout);

    /**
     * Add new Admin (POST Method)
     */
    app.post('/users/users', passport.authenticate('local-signup', {
        successRedirect: '/logout',
        failureRedirect: '/admin/users',
        failureFlash: true
    }));

    /**
     * SignIn (POST Method)
     */
    app.post('/login', passport.authenticate('local-signin', {
        successRedirect: '/admin',
        failureRedirect: '/login',
        failureFlash: true
    }));
};