let indexController = require('../controllers/mainIndexController.js');

module.exports = function(app) {

    /**
     * (GET Method)
     */
    app.get('/', indexController.index);

};