let bCrypt = require('bcrypt-nodejs');

module.exports = function(passport, user) {

    let User = user;
    let LocalStrategy = require('passport-local').Strategy;

    let theUser = null;

    /*let models = require("../../models"),
        Roles = models.roles;*/

    /**
     * Local SignUp Users
     */
    passport.use('local-signup', new LocalStrategy({

            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },function(req, email, password, done) {

            let generateHash = function(password) {
                return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
            };

            User.findOne({
                where: {
                    email: email
                }
            }).then(function(user) {

                if (user) {
                    return done(null, false, req.flash('signupMessage', 'That email is already taken !'));

                } else {
                    let userPassword = generateHash(password);

                    let data = {
                        email: email,
                        password: userPassword,
                    };

                    User.create(data).then(function(newUser, created) {

                        if (!newUser) {
                            return done(null, false);
                        }

                        if (newUser) {
                            theUser = User;
                            return done(null, newUser);
                        }
                    });
                }
            });
        }
    ));


    /**
     * Local SignIn Admin
     */
    passport.use('local-signin', new LocalStrategy({

            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        }, function(req, email, password, done) {

            let User = user;
            let isValidPassword = function(userpass, password) {

                return bCrypt.compareSync(password, userpass);
            }

            User.findOne({
                where: {
                    email: email
                }
            }).then(function(user) {

                if (!user) {
                    return done(null, false, req.flash('loginMessage', 'Email does not exist !'));
                }

                if (!isValidPassword(user.password, password)) {
                    return done(null, false, req.flash('loginMessage', 'Incorrect password !'));
                }

                if(JSON.parse(JSON.stringify(user)).status === 'inactive') {
                    return done(null, false, req.flash('loginMessage', 'Oops! Account not activated yet.'));
                }

                let userinfo = user.get();
                theUser = User;
                return done(null, userinfo);

            }).catch(function(err) {

                console.log("Error:", err);
                return done(null, false, req.flash('loginMessage', 'Something went wrong with your Signin !'));
            });
    }));
    /**
     * Serialize User into session
     */
    passport.serializeUser(function(user, done) {

        done(null, user.id);
    });

    /**
     * Deserialize user
     */
    passport.deserializeUser(function(id, done) {

        theUser.findById(id).then(function(user) {

            updateLastLogin(id, user);

            if (user) {
                done(null, user.get());
            } else {
                done(user.errors, null);
            }
        });
    });

    /**
     * Set Up Last_Login for the User
     *
     * @param _id   user id
     * @param user  user row
     */
    function updateLastLogin(_id, user) {
        user.update({last_login: Date.now()},
            {where: {id: _id}}).then().catch(err => console.log("Faild to Update Last login: " + err))
    }
};