var exports = module.exports = {};

exports.countriesEurope = [
    'France',
    'Finland',
    'Germany',
    'Ireland',
    'Italy',
    'Portugal',
    'Spain',
    'Sweden',
];

exports.countriesAfrica = [
    'Algeria',
    'Cote d\'Ivoire',
    'Egypt',
    'Madagascar',
    'Morocco',
    'South Africa',
    'Tunisia',
];