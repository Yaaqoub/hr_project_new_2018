let bCrypt = require('bcrypt-nodejs');

let generateHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
};

console.log(generateHash(""));