var exports = module.exports = {};

let bCrypt = require('bcrypt-nodejs');

exports.admin = {
    email: process.env.PROD_ADMIN_USER,
    password: bCrypt.hashSync(process.env.PROD_ADMIN_PASS, bCrypt.genSaltSync(8), null),
    status: 'active'
};