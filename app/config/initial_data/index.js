let fs = require("fs");
let path = require("path");
let camelCase = require('lodash/camelCase');
let data = {};

module.exports = function(models) {

    let User = models.users,
        Resources = models.resources,
        Countries = models.countries,
        Regions = models.regions,
        Roles = models.roles;

    fs.readdirSync(path.join(__dirname)).filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js") && (file !== "generatePasswords.js");
    }).forEach(function(file) {
        let prop = camelCase(file.slice(0, -3));
        let Resource = require(`./${file}`);
        this[prop] = Resource;
    });


    Roles.bulkCreate([roles.roleAdmin]);

    regions.regionsList.forEach((region) => {
        Regions.create({region_name: region}).then((reg) => {

            if (region === 'Europe') {
                countries.countriesEurope.forEach(function (value) {
                    Countries.create({
                        country_name: value
                    }).then((country) => {
                        reg.setCountries(country);
                    });
                });
            }

            if (region === 'Africa') {
                countries.countriesAfrica.forEach(function (value) {
                    Countries.create({
                        country_name: value
                    }).then((country) => {
                        reg.setCountries(country);
                    });
                });
            }
        });
    });

    Roles.findOne({
        where: {
            role_name: 'Admin'
        }
    }).then(function (role) {
        User.create(users.admin).then(function (user) {
            user.addUserHasRole(role);
        });

        resources.admin.forEach(function (value) {
            Resources.create({resource_name: value}).then((resource) => {
                role.addRoleHasResource(resource);
            });
        });
    });
};