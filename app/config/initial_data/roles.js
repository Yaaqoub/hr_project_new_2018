var exports = module.exports = {};

/**
 * Don't modify this role, Mobinergy Admin
 * @type {{role_name: string}}
 */
exports.roleAdmin = {
    role_name: 'Admin'
};