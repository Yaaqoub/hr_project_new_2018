var exports = module.exports = {};

exports.admin = ["human_resources", "personal_administration", "organisation",
                "payroll", "reports", "configuration",
                "finance", "accounting", "treasury",
                "finance_reports", "administration", "employees_management",
                "users_management", "roles_management", "general_reports"];