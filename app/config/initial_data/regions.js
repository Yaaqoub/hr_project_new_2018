var exports = module.exports = {};

exports.regionsList = [
    'Africa',
    'Asia',
    'Europe',
    'Middle East',
    'Central America',
    'North America',
    'South America'
];