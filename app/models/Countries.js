module.exports = function(sequelize, Sequelize) {

    let Countries = sequelize.define('countries', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        country_name: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Countries;
};