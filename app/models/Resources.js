module.exports = function(sequelize, Sequelize) {

    let Resources = sequelize.define('resources', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        resource_name: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Resources;
};