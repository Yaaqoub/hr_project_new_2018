module.exports = function(sequelize, Sequelize) {

    let Regions = sequelize.define('regions', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        region_name: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Regions;
};