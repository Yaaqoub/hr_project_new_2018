module.exports = function(sequelize, Sequelize) {

    let LeaveType = sequelize.define('leavetype', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        color: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        use_allowance: {
            type: Sequelize.TINYINT,
            defaultValue: '0'
        }
    },{
        underscored: true
    });

    return LeaveType;
}