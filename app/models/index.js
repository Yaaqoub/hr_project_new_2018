"use strict";

let fs = require("fs");
let path = require("path");
let Sequelize = require("sequelize");
let env = process.env.NODE_ENV || "development";
let config = require(path.join(__dirname, '..', 'config', 'config.js'))[env];
let sequelize = new Sequelize(config.database, config.username, config.password, config);
let db = {};


fs.readdirSync(__dirname).filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function(file) {
        let model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});


db.sequelize = sequelize;
db.Sequelize = Sequelize;

/**
 * Tables relations START
 */
db.users.belongsToMany(db.roles, {through: 'users_roles', foreignKey: 'user_id', as:'userHasRole'});

db.messages.belongsTo(db.users, {as: 'sent_from'}); //Messages
db.messages.belongsTo(db.users, {as: 'sent_to'}); //Messages

db.jobs.hasMany(db.employees, {foreignKey: 'job_id'});

db.employees.belongsTo(db.jobs, {foreignKey: 'job_id'});
db.employees.belongsTo(db.users, {foreignKey: 'user_id'});
db.employees.hasMany(db.leave, {foreignKey: 'employee_id'});
db.employees.hasOne(db.leave, {foreignKey: 'approver_id'});
db.employees.belongsTo(db.departments, {foreignKey: 'department_id'});

db.countries.belongsTo(db.regions, {foreignKey: 'region_id'});
db.countries.hasMany(db.company, {foreignKey: 'country_id'});
db.countries.hasMany(db.locations, {foreignKey: 'country_id'});

db.regions.hasMany(db.countries, {foreignKey: 'region_id'});

db.roles.belongsToMany(db.users, {through: 'users_roles', foreignKey: 'role_id', as:'roleHasUser'});

db.departments.hasMany(db.employees, {foreignKey: 'department_id'});
db.departments.belongsTo(db.company, {foreignKey: 'company_id'});
db.departments.belongsTo(db.locations, {foreignKey: 'location_id'});

db.company.hasMany(db.departments, {foreignKey: 'company_id'});
db.company.belongsTo(db.countries, {foreignKey: 'country_id'});
db.company.hasMany(db.clientdomains, {foreignKey: 'company_id'});

db.locations.hasOne(db.departments, {foreignKey: 'location_id'});
db.locations.belongsTo(db.countries, {foreignKey: 'country_id'});

db.leave.belongsTo(db.leavetype, {foreignKey: 'leave_type_id'});
db.leave.belongsTo(db.employees, {foreignKey: 'employee_id'});

db.leavetype.hasMany(db.leave, {foreignKey: 'leave_type_id'});

db.clientdomains.belongsTo(db.company, {foreignKey: 'company_id'});

//Permissions
//db.roles.hasMany(db.rolePermissionResource, {foreignKey: 'role_id', targetKey: 'role_id'});

//db.permissions.hasMany(db.rolePermissionResource, {foreignKey: 'permission_id', targetKey: 'permission_id'});

//db.resources.hasMany(db.rolePermissionResource, {foreignKey: 'resource_id', targetKey: 'resource_id'});
db.resources.belongsToMany(db.roles, {through: 'Resources_Roles', foreignKey: 'resources_id', as:'resourceHasRole'});
db.roles.belongsToMany(db.resources, {through: 'Resources_Roles', foreignKey: 'role_id', as:'roleHasResource'});

//db.rolePermissionResource.belongsTo(db.roles, {foreignKey: 'role_id'});
//db.rolePermissionResource.belongsTo(db.permissions, {foreignKey: 'permission_id'});
//db.rolePermissionResource.belongsTo(db.resources, {foreignKey: 'resource_id'});

/**
 * Tables relations END
 */

module.exports = db;