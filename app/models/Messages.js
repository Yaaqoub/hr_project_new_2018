module.exports = function(sequelize, Sequelize) {

    let Messages = sequelize.define('messages', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        subject: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        body: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        sent_at: {
            type: Sequelize.DATE
        }
    },{
        underscored: true
    });

    return Messages;
}