module.exports = function(sequelize, Sequelize) {

    let Locations = sequelize.define('locations', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        street_address: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        postal_code: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        city: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        state_province: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Locations;
};