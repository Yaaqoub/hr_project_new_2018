module.exports = function(sequelize, Sequelize) {

    let Departments = sequelize.define('departments', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        department_name: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Departments;
};