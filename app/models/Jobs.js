module.exports = function(sequelize, Sequelize) {

    let Jobs = sequelize.define('jobs', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        job_title: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        min_salary: {
            type: Sequelize.DECIMAL,
            notEmpty: true
        },

        max_salary: {
            type: Sequelize.DECIMAL,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Jobs;
}