module.exports = function(sequelize, Sequelize) {

    let Employees = sequelize.define('employees', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        firstname: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        lastname: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        email: {
            type: Sequelize.STRING,
            validate: {
                isEmail: true
            }
        },

        phone_number: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        date_of_birth: {
            type: Sequelize.DATE
        },

        sec_soc_num: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        is_contractor: {
            type: Sequelize.ENUM('yes', 'no'),
            defaultValue: 'yes'
        },

        hire_date: {
            type: Sequelize.DATE
        },

        salary: {
            type: Sequelize.DECIMAL,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return Employees;
}