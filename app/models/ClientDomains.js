module.exports = function(sequelize, Sequelize) {

    let ClientDomains = sequelize.define('clientdomains', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        domain_name: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    },{
        underscored: true
    });

    return ClientDomains;
};