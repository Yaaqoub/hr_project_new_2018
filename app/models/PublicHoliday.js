module.exports = function(sequelize, Sequelize) {

    let PublicHoliday = sequelize.define('publicholiday', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        start_date: {
            type: Sequelize.DATE
        },

        end_date: {
            type: Sequelize.DATE
        }
    },{
        underscored: true
    });

    return PublicHoliday;
}