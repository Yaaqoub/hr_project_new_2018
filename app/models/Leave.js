module.exports = function(sequelize, Sequelize) {

    let Leave = sequelize.define('leave', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        status: {
            type: Sequelize.ENUM('APPROVED', 'DISAPPROVED'),
            defaultValue: 'DISAPPROVED'
        },

        employee_comment: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        approver_comment: {
            type: Sequelize.STRING,
            notEmpty: true
        },

        decided_at: {
            type: Sequelize.DATE
        },

        date_start: {
            type: Sequelize.DATE
        },

        date_end: {
            type: Sequelize.DATE
        },
    },{
        underscored: true
    });

    return Leave;
}